CC=g++

define clean
    rm -f src/*.o
endef

serverSide: src/server_chat.cpp
	mkdir -p build
	$(CC) src/server_chat.cpp -o build/server_chat -pthread
	$(clean)

clientSide: src/client_chat.cpp
	mkdir -p build
	$(CC) src/client_chat.cpp  -o build/client_chat -pthread
	$(clean)

clean:
	rm -f src/*.o
