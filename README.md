Universidade Federal da Bahia - SLS 2020
MATA59 - Redes de computadores 1
Discentes - Karen Santos, Adriana Silva, Ingrid Rodrigues, Lisandra Calmnon, João Pedro Nunes


Descritivo:
Projeto avaliativo da disciplina MATA59 - Redes 1. Consite na implementação de um sistema de bate-papo, baseado na topologia de rede estrela, onde um servidort central lida com a conxeção e troca de mensagens.


Para executar o programa você deve rodar os comandos:
- make serverSide && make clientSide
- ./build/server_chat <port>
- ./build/client_chat <client_name> <endereço_de_ip> <port>

A lista de comandos suportados:
- SEND <MESSAGE>: Envia <MESSAGE> para todos os clientes conectados.
- SENDTO <CLIENT_NAME> <MESSAGE>: Envia <MESSAGE> para o cliente <CLIENT_NAME>
- WHO: Retorna a lista dos clientes conectados ao servidor
- HELP: Retorna a lista de comandos suportados e seu uso

O Protocolo utilizado para o envio e recebimento de mensagens é o protocolo Socket, que provê a comunicação entre duas pontas (fonte e destino) desde que ambos  que estejam na mesma máquina (Unix Socket) ou na rede (TCP/IP Sockets). Para a implementação da atividade utilizamos o modelo de sockets de fluxo para uma aplicação cliente/servidor onde os dados são transferidos como um fluxo de bytes. Ambos (cliente e servidor) fazem uso da mesma API de sockets para a comunicação. 

O Servidor utiliza a mesma API de sockets do Cliente, inicialmente criando um socket só que esse socket precisa assocar o socket a uma porta do sistema operacional, e depois utilizar o listen() para escutar novas conexões de clientes nessa porta. Quando um novo cliente se conecta, a chamada accept() é utilizada para começar a se comunicar. O servidor fica em um loop no while(1) recebendo e enviando mensagens através do par de funções send() e recv().

O Cliente cria um socket e logo em seguida se conecta ao servidor e inicia um loop no while(1) que fica fazendo send() e recv() com as mensagens específicas da aplicação. Quando a mensagem "exit" é enviada o cliente está informando ao servidor é o momento de terminar a conexão, o servidor então chama a função close() pra finalizar o socket.
