#include "serverlibrary.h"
// #include "server_function.h"
using namespace std;

/*******************************************************************
*   TYPEDEFS
*******************************************************************/
typedef struct{
    string nome;
	int sockID;
	int index;
	struct sockaddr_in clientAddr;
	int len;

} client;

/*******************************************************************
*   GLOBAL VARIABLES
*******************************************************************/
vector<pthread_t> threads;
int qtdThreads;
vector<client> clientes;
int qtdAtualClientes;

vector<int>::iterator it;

/*******************************************************************
*                   FUNÇÕES AUXILIARES(SUPORTE)
*******************************************************************/
/*******************************************************************
*******************************************************************/
/*******************************************************************
*   FUNÇÃO AJUDA 
*******************************************************************/
/**
 * Todos os comandos e como utilizá-los são postos em char e enviados.
 * 
*/

bool ajudeMe(client solicitante) {
    
    char comandos[1500];
    strcat(comandos,"HELP: Retorna a lista de comandos suportados e seu uso\n");
    strcpy(comandos, "SEND <MESSAGE>:  Envia <MESSAGE> para todos os clientes conectados.\n");
    strcat(comandos,"SENDTO <CLIENT_NAME> <MESSAGE>:  Envia <MESSAGE> para o cliente <CLIENT_NAME>\n");
    strcat(comandos,"WHO: Retorna a lista dos clientes conectados ao servidor\n");
    
    send(solicitante.sockID, comandos, strlen(comandos), 0);
    return true;
}

/*******************************************************************
*   FUNCTION DE LISTAGEM DE USUÁRIOS 
*******************************************************************/
/**
 * @param solicitante - struct do tipo client, que solicitou a listagem 
*/
bool quemEstaOnline(client solicitante) {
    char clientesConectados[2000];
    memset(&clientesConectados, 0, sizeof(clientesConectados));
    for (client cliente : clientes) {
        char * name = new char(cliente.nome.length()+1);
        memcpy(name,cliente.nome.c_str(), cliente.nome.length() + 1);
        strcat(clientesConectados,name);
        strcat(clientesConectados,"\n");
    }
    cout<<"Usuário conectados: \n" <<endl;
    send(solicitante.sockID, clientesConectados, strlen(clientesConectados), 0);
    return true;
}



/*******************************************************************
*                  FUNÇÕES DE AÇÕES POSSÍVEIS 
*******************************************************************/
/*******************************************************************
/*******************************************************************
*   FUNCTION DE ENVIO DE MENSAGEM DIRETA
*******************************************************************/
/**
 * faz envio de mensagem no chat geral - visualizado por todos
 * @param buffer - contem a mensagem
 * @param emissor - struct do tipo client, cliente que solicita o envio
*/
bool sendMessage(char * buffer, client emissor) {
    for (client cliente : clientes) {
        /*tratativa de validação para não haver envio para si mesmo 
        * não envia para o cliente emissor, apenas para os outros*/
        if ((cliente.nome).compare(emissor.nome) != 0) {
            send(cliente.sockID, buffer, strlen(buffer), 0);
        }
    }
    return true;
}

/*******************************************************************
*   FUNCTION DE ENVIO DE MENSAGEM COM DESTINATÁRIO DEFINIDO
*******************************************************************/
/**
 * faz o envio direcionado da mensagem ao usuário indicado
 * @param buffer - contem a mensagem
 * @param emissor - struct do tipo cliente, que solicita o envio
 * @param receptor - string de nome do receptor
*/
bool sendMessageTo(char * buffer, string destinatario, client emissor) {
    for (client cliente : clientes) {
        /*filtra clientes e localizar o destinarario para fazer o envio*/
        if ((cliente.nome).compare(destinatario) == 0) {
            send(cliente.sockID, buffer, strlen(buffer), 0);
            return true;
        }
    }
    cout << "ERRO: Não foi possível localizar destinatário" <<endl;
    return false;
}


/*******************************************************************
*   FUNCTION DE ESTABELECIMENTO DE CONEXÃO DO CLIENTE
*******************************************************************/
/**
 * @param args - ponteiro de argumentos
*/

void * connectClient(void * args) {
    client* cliente = (client*) args;
    string nome = cliente->nome;
    int sockID = cliente->sockID;

    char buffer[1500];
    int bytesRead = 0;
    int bytesWritten = 0;
    char comando[10];

    while(1){
        bool result = false;
        struct tm *horario;
        time_t segundos;
        time(&segundos);
        horario = localtime(&segundos);

        /* Ação: Limpeza de BUffer */
        memset(&buffer, 0, sizeof(buffer));
        int err = recv(sockID, buffer, 2000, 0);
        if(err == -1){
            cout << "Algo impediu a leitura do buffer."<< endl;
        }
        
        if(!strcmp(buffer, "exit")){
           cout << "O cliente não está mais na sessão." << endl;
            break;
        }

        char * palavra = strtok(buffer, " ");
        strcpy(comando, palavra);

        if (strcmp(comando,"SEND") == 0) {

            char msg[2000];
            memset(&msg, 0, sizeof(msg));
            for ( int i = 0; i < nome.length(); i++) {
                msg[i] = nome[i];
            }
            msg[nome.length()] = ':';
            msg[nome.length() + 1] = ' ';
            while(1) {
                palavra = strtok(NULL, " ");
                if(palavra == NULL) {
                    break;
                }
                strcat(msg, palavra);
                strcat(msg, " ");
            }
            result = sendMessage(msg,*cliente);

        } else if (strcmp(comando, "SENDTO") == 0) {
            char nomeDest[40];
            cout<< "comando digitado " << comando<<endl;
            palavra = strtok(NULL, " ");
            cout<< "palavra que veio:  " << palavra<<endl;
            
            if (palavra == NULL || strcmp(palavra,"") != 0) {
                char msgErro[] = "ERRO: Informe comando, destinatário e mensagem";
                send(cliente->sockID, msgErro, strlen(msgErro), 0);
                continue;
            }
                                   
            strcpy(nomeDest,palavra); /** salvar o nome do destinatário**/

            char msg[2000];
            memset(&msg, 0, sizeof(msg));
            for ( int i = 0; i < nome.length(); i++) {
                msg[i] = nome[i];
            }
            msg[nome.length()] = ':';
            msg[nome.length() + 1] = ' ';
            while(1) {
                palavra = strtok(NULL, " ");
                if(palavra == NULL) {
                    break;
                }
                strcat(msg, palavra);
                strcat(msg, " ");
            }
            result = sendMessageTo(msg, nomeDest, *cliente);     

        } else if (strcmp(comando, "WHO") == 0) {
            result = quemEstaOnline(*cliente);

        } else if (strcmp(comando, "HELP") == 0) {
            result = ajudeMe(*cliente);

        } else {
            char msgErro[] = "ERRO: Não foi possivel encontrar este comando. Em caso de dúvidas digite HELP! \n ";
            send(cliente->sockID, msgErro, strlen(msgErro), 0);
        }

        cout << horario->tm_hour <<":"<< horario->tm_min << "   " << nome << " "<< comando << " Executado: ";
        if (result) {
            cout << "Sim" << endl;
        } else {
            cout << "Não" << endl;
        }
    }
    return NULL;
}

int main(int argc, char *argv[]){
    if(argc != 2){
        cerr << "ERRO: Argumento inválido. Favor informar número da porta (EX: ./build/server_chat 12345 )" << endl;
        exit(0);
    }
    struct tm *horario;
    time_t segundos;
    time(&segundos);
    horario = localtime(&segundos);

    
    /*
    * TRATATIVA DE THREAD
    * redireciona o argumento da porta do servidor
    */
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    /**
     * port - salva portal de conexão local 
     * buffer - Buffer de armazenamento temporário da mensagens trocadas
     */
    int port = atoi(argv[1]);
    char buffer[1500];
    
    /*
     * CONFIGURAÇÕES DO SOCKET
     * Define leitura do endereço de redes
     */
    sockaddr_in servAddr;
    bzero((char*)&servAddr, sizeof(servAddr));
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servAddr.sin_port = htons(port);

    /*
     * TRATATIVA DE ERRO DE CONEXÃO
    */
    int serverSd = socket(AF_INET, SOCK_STREAM, 0);
    if(serverSd < 0){
        cerr << "Erro ao estabelecer socket do servidor" << endl;
        exit(0);
    }

    int bindStatus = bind(serverSd, (struct sockaddr*) &servAddr, 
        sizeof(servAddr));
    if(bindStatus < 0){
        cerr << "Erro ao tentar conectar o socket ao endereço local" << endl;
        exit(0);
    }
    cout << "Aguardando conexão de algum cliente..." << endl;
    listen(serverSd, 5);
    sockaddr_in newSockAddr;
    socklen_t newSockAddrSize = sizeof(newSockAddr);

    struct timeval start1, end1;
    int newSd;
    while(1) {

        newSd = accept(serverSd, (sockaddr *)&newSockAddr, &newSockAddrSize);
        if(newSd < 0){
            cerr << "Erro ao aceitar requisição do cliente!" << endl;
            exit(1);
        }
        char nomeCliente[1500];
        
        memset(&nomeCliente, 0, sizeof(nomeCliente));
        recv(newSd, (char*)&nomeCliente, sizeof(nomeCliente), 0);

        
        struct find_element{
            string nomeCliente;
            find_element(string nomeCliente) : nomeCliente(nomeCliente) {}
            bool operator () (const client& ache) const{
                return nomeCliente == ache.nome;
            }
        };
        
        if (!(clientes.end() == find_if( clientes.begin(), clientes.end(), find_element(nomeCliente)))){
            char msgErro[] = "ERRO: Nome em uso. Não foi possível se conectar ao servidor.";
            send(newSd, msgErro, sizeof(msgErro), 0);
            continue;
            
        } else {
            /*
            *conexão estabelecida com sucesso
            */
            char msgSucesso[] = "S";
            send(newSd, msgSucesso, sizeof(msgSucesso), 0);
            
        }
    
        /*
        * cria novo cliente e adiciona a lista de usuários ativos
        * uma vez que o nome seja utilziado, não fica mais disponível
        */
        
        client cliente;
        cliente.nome = nomeCliente;
        cliente.clientAddr = newSockAddr;
        cliente.sockID = newSd;
        cliente.len = newSockAddrSize;
        clientes.push_back(cliente);

        cout << horario->tm_hour <<":"<<horario->tm_min<<" "<< nomeCliente <<" Conectado"<< endl;

        gettimeofday(&start1, NULL);
        int bytesRead, bytesWritten = 0;

        pthread_t novaThread;
        threads.push_back(novaThread);
        qtdThreads ++;
        pthread_create(&(threads[qtdThreads - 1]),NULL, connectClient, &(clientes[clientes.size() - 1]));
    }

    int testReturn = 0;
    for (int i = 0; i < qtdThreads; i++) {
        testReturn += pthread_join(threads[i], NULL);
    }
    if (testReturn != 0) {
        cout << "Erro no join..." << endl;
    }

    gettimeofday(&end1, NULL);
    close(newSd);
    close(serverSd);
    cout << "Fim da conexão..." << endl;

    return 0;   
}
