#include "clientlibrary.h"

using namespace std;

/*******************************************************************
*   FUNÇÃO P/ RECEBER E MOSTRAR MENSAGENS VINDAS DO SERVIDOR
*******************************************************************/
/**
 * recebe mensagens do servidor
 * mostra mensagens recebidas do servidor
 * @param args - ponteiro de argumentos
*/

void * receiveMessageServer(void * args) {   
    int clientSide = *((int *) args);
    char message[1500];
    while(1) {
        int bytesRead = 0;
        memset(&message, 0, sizeof(message)); 
        bytesRead += recv(clientSide, (char*)&message, sizeof(message), 0);
        if(!strcmp(message, "exit")){
            cout << "O servidor encerrou a sessão." << endl;
            break;
        }
        cout << message << endl;
    }
    return NULL;
}

int main(int argc, char *argv[]){
    /*nome de usuário, nome/ip servidor, porta de conexão*/
    if(argc != 3){
        cerr << "Uso: nome do cliente endereço de IP porta" << endl; exit(0); 
    } 
    
    /**
     * serverAddIp - salva endereço de servidor
     * nomeClienteChat - salva nome do usuário 
     * buffer - Buffer de armazenamento temporário da mensagens trocadas
     */ 
    char *serverAddIp = argv[2]; int port = atoi(argv[3]); 
    char * nomeClienteChat = argv[1];
    char buffer[1500]; 

    struct hostent* host = gethostbyname(serverAddIp); 
    sockaddr_in sendSockAddr;   
    bzero((char*)&sendSockAddr, sizeof(sendSockAddr)); 
    sendSockAddr.sin_family = AF_INET; 
    sendSockAddr.sin_addr.s_addr = 
        inet_addr(inet_ntoa(*(struct in_addr*)*host->h_addr_list));
    sendSockAddr.sin_port = htons(port);
    int clientSide = socket(AF_INET, SOCK_STREAM, 0);
    int status = connect(clientSide,
                         (sockaddr*) &sendSockAddr, sizeof(sendSockAddr));
    if(status < 0){
        cout<<"Erro de conexao com socket!"<<endl;
        exit(1);
    }
    
    char nome[25];
    strcpy(nome,nomeClienteChat);
    /*
    * Troca de mensagem para o servidor - envio de nome de usuário
    * */
    send(clientSide, (char*)&nome, sizeof(nome), 0);

    char alertConex[100];
    memset(&alertConex, 0, sizeof(alertConex));//apagar o buffer
    recv(clientSide, (char*)&alertConex, sizeof(alertConex), 0);
    char msgSucesso[] = "S";
    if (!strcmp(alertConex, msgSucesso)) {
        cout << "Conexão estabelecida com sucesso!" << endl;
    } else {
        cout << alertConex << endl;
        exit(1);
    }
    int bytesRead, bytesWritten = 0;

    //thread para receber sempre mensagens do server
    pthread_t threadMsgs;
    pthread_create(&threadMsgs, NULL, receiveMessageServer, &clientSide);

    while(1){   
        string data;
        /**
         * aguarda entrada de dados 
         */
        getline(cin, data);
        cout<<data<<endl;

        /*
        * 
        * trata limite maximo do buffer de mensagem
         */
        if (data.length() > 1500) {
            cout << "ERRO: O tamanho da mensagem é maior que 1500 caracteres" << endl;
            continue;
        }

        memset(&buffer, 0, sizeof(buffer));
        strcpy(buffer, data.c_str());
        if(data == "exit"){
            cout << "Fim da conexão com servidor. Chat encerrado!" << endl;
            send(clientSide, buffer, 1500, 0);
            break;
        }
        send(clientSide, buffer, 1500, 0);
    }
    pthread_join(threadMsgs, NULL);

    close(clientSide);
    cout << "Fim da conexão." << endl;
    return 0;    
}
